package nasaTesting

import (
	"NasaProject/handlers"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestIDHandler(t *testing.T) {
	//GETs
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Id/2004953", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := handlers.IDHandler("64NQkSQzqktiHMecqzfS0cFTGA89WyTKsFDb6gx3")
	handler.ServeHTTP(rr, req)
	//checks status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	//compares strings
	if !strings.Contains(rr.Body.String(), `"id":"2004953","name":"4953 (1990 MU)","absolute_magnitude":14.1`) {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}
