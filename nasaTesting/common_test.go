package nasaTesting

import (
	"NasaProject/functionality"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

//TestGetDates ...
func TestGetDates(t *testing.T) {
	//GETs
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Neo/?start_date=2019-01-01=&end_date=2019-01-08", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	myStartDate, myEndDate := functionality.GetDates(req)
	//checks status code
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	//compare start date
	if strings.Compare(myStartDate, req.URL.Query()["start_date"][0]) != 0 {
		t.Errorf("Could not find correct substring from endpoint.")
	}
	//compare end date
	if strings.Compare(myEndDate, req.URL.Query()["end_date"][0]) != 0 {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}

//TesdDangerZone ..
func TestDangerZone(t *testing.T) {
	myDangerZone := functionality.DangerZone(319.5618867213, 714.5621017269, "27.5370875653", "51588462.186032492")

	if myDangerZone != 0.0027662923464307713 {
		t.Errorf("ERROR dangerzone value is wrong")
	}
}

//TestGetID ...
func TestGetID(t *testing.T) {
	//GETs
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Id/2004953", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	myID := functionality.GetID(req)
	//checks status code
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	if strings.Compare(myID, req.URL.Path[17:]) != 0 {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}
