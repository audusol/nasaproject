package nasaTesting

import (
	"NasaProject/handlers"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestNeoHandler(t *testing.T) {
	//GETs
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Neo/?start_date=2019-09-05&end_date=2019-09-10", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := handlers.NeoHandler("64NQkSQzqktiHMecqzfS0cFTGA89WyTKsFDb6gx3")
	handler.ServeHTTP(rr, req)
	//Checks status code
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	//Compares strings
	if !strings.Contains(rr.Body.String(), `"list_asteroids":[{"id":"3843796","date":"2019-09-05","danger":2.4853257106927535`) {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}
