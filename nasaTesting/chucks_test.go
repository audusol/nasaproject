package nasaTesting

import (
	"NasaProject/handlers"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
)

func TestChuckHander(t *testing.T) {
	//GETs
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Chuck/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := handlers.ChuckHandler()

	// Our handlers satisfy http.Handler, so we can call their ServeHTTP method
	// directly and pass in our Request and ResponseRecorder.
	handler.ServeHTTP(rr, req)

	//checks status code
	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	//compares strings
	if !strings.Contains(rr.Body.String(), `"icon_url":"https://assets.chucknorris.host/img/avatar/chuck-norris.png"`) {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}

func TestChuckCategoryHander(t *testing.T) {
	req, err := http.NewRequest("GET", "/nasaSquad/v1/Chuck/animal", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := handlers.ChuckHandler()

	handler.ServeHTTP(rr, req)

	status := rr.Code
	if status != http.StatusOK {
		t.Errorf("Handler returned unexpected body: got %v want %v", status, http.StatusOK)
	}
	if !strings.Contains(rr.Body.String(), `icon_url":"https://assets.chucknorris.host/img/avatar/chuck-norris.png"`) {
		t.Errorf("Could not find correct substring from endpoint.")
	}
}
