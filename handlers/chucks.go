package handlers

import (
	"NasaProject/functionality"
	"encoding/json"
	"net/http"
)

//ChuckHandler ...
func ChuckHandler() http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {

		var chuckData functionality.ChuckData
		var chuckJoke functionality.ChuckJoke
		chuckCategory := r.URL.Path[20:]
		if r.Method == http.MethodGet {
			//Random joke from a given category
			if chuckCategory != "" {
				resp, err := http.Get("https://api.chucknorris.io/jokes/random?category=" + chuckCategory)
				if err != nil {
					http.Error(w, "Faulty Response, try again", http.StatusBadRequest)
				}
				err = json.NewDecoder(resp.Body).Decode(&chuckData)
				if err != nil {
					http.Error(w, "Decode failed, try again", http.StatusBadRequest)
				}

			} else {
				//Random chuck norris joke
				resp, err := http.Get("https://api.chucknorris.io/jokes/random")
				if err != nil {
					http.Error(w, "Faulty Response, try again", http.StatusBadRequest)
				}
				err = json.NewDecoder(resp.Body).Decode(&chuckData)
				if err != nil {
					http.Error(w, "Decode failed, try again", http.StatusBadRequest)
				}
			}
			chuckJoke.Categories = chuckData.Categories
			chuckJoke.IconURL = chuckData.IconURL
			chuckJoke.ID = chuckData.ID
			chuckJoke.URL = chuckData.URL
			chuckJoke.Value = chuckData.Value

			w.Header().Add("content-type", "application/json")
			err := json.NewEncoder(w).Encode(chuckJoke)
			if err != nil {
				http.Error(w, "Encode failed, try again", http.StatusBadRequest)
			}
		}
	}
	return http.HandlerFunc(f)
}
