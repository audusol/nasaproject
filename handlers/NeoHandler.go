package handlers

import (
	"NasaProject/functionality"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
)

//NeoHandler function, takes string from main, is of type Handler. Returns a handlefunc.
func NeoHandler(key string) http.Handler {

	f := func(w http.ResponseWriter, r *http.Request) {

		//Checks if method equals GET
		if r.Method == http.MethodGet {
			var printStruct functionality.Encoding
			var allInfo functionality.AsteroidList
			startDate, endDate := functionality.GetDates(r)

			resp, err := http.Get(fmt.Sprintf("https://api.nasa.gov/neo/rest/v1/feed?start_date=%s&end_date=%s&api_key=%s", startDate, endDate, key))
			if resp != nil {
				defer resp.Body.Close()
			}
			if err != nil {
				http.Error(w, "Failed to retrieve information from the NASA API", http.StatusInternalServerError)
				return
			}

			//If the status code is OK, do your thing
			if resp.StatusCode == 200 {
				// decodes into the struct
				err = json.NewDecoder(resp.Body).Decode(&allInfo)
				if err != nil {
					http.Error(w, "Encoding failed, try again", http.StatusBadRequest)
				}

				for i, j := range allInfo.NearEarthObject {

					for k := 0; k < len(j); k++ {
						var asteroidStruct functionality.InfoAsteroids

						asteroidStruct.ID = allInfo.NearEarthObject[i][k].ID
						asteroidStruct.Danger = functionality.DangerZone(
							allInfo.NearEarthObject[i][k].EstimatedDiameter.Meters.EstimatedDiameterMax,
							allInfo.NearEarthObject[i][k].EstimatedDiameter.Meters.EstimatedDiameterMin,
							allInfo.NearEarthObject[i][k].CloseApproachData[0].RelativeVelocity.KilometersPerSecond,
							allInfo.NearEarthObject[i][k].CloseApproachData[0].MissDistance.Kilometers,
						)

						asteroidStruct.Date = allInfo.NearEarthObject[i][k].CloseApproachData[0].CloseApproachDate
						asteroidStruct.Lunar = allInfo.NearEarthObject[i][k].CloseApproachData[0].MissDistance.Lunar
						asteroidStruct.Kilometers = allInfo.NearEarthObject[i][k].CloseApproachData[0].MissDistance.Kilometers

						printStruct.ListAsteroids = append(printStruct.ListAsteroids, asteroidStruct)
					}

				}

				// sorter her
				sort.Slice(printStruct.ListAsteroids, func(i, j int) bool {
					return printStruct.ListAsteroids[i].Danger > printStruct.ListAsteroids[j].Danger
				})

				w.Header().Add("content-type", "application/json")
				err = json.NewEncoder(w).Encode(printStruct)
				if err != nil {
					http.Error(w, "Decoding failed, try again", http.StatusBadRequest)
				}
			} else {
				http.Error(w, "Malformed request to NASA api, please refer to the user manual on how to format requests.", http.StatusBadRequest)
			}

		} else {
			http.Error(w, "Http Method not allowed, Get only", http.StatusBadRequest)
		}

	}
	return http.HandlerFunc(f)
}
