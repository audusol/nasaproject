package handlers

import (
	"NasaProject/functionality"
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
)

//IDHandler is of type Handle, takes a string argument from main containing the key for the API. Returns a handlefunc.
func IDHandler(key string) http.Handler {
	f := func(w http.ResponseWriter, r *http.Request) {
		//Checks if httpMethod is equal to GET
		if r.Method == http.MethodGet {
			ID := functionality.GetID(r)

			//Sends request to API closes body after, also returns error if it could not retrieve info.
			resp, err := http.Get(fmt.Sprintf("https://api.nasa.gov/neo/rest/v1/neo/%s?api_key=%s", ID, key))
			if resp != nil {
				defer resp.Body.Close()
			}
			if err != nil {
				http.Error(w, "Failed to retrieve information from the NASA API", http.StatusInternalServerError)
				return
			}
			//Checks if return code equals 200, if not returns error.
			if resp.StatusCode == 200 {

				var Asteroid functionality.IDAsteroid

				// Decodes into the struct, sends error if failed.
				err = json.NewDecoder(resp.Body).Decode(&Asteroid)
				if err != nil {
					http.Error(w, "Encoding failed, try again", http.StatusBadRequest)
				}

				//Initialize variables for data comparisons.
				var asteroidStruct functionality.InfoIDAsteroids
				var approachData functionality.IDApproachData

				//Gets initial values for the asteroid
				asteroidStruct.ID = Asteroid.ID
				asteroidStruct.AbsoluteMagnitude = Asteroid.AbsoluteMagnitude
				asteroidStruct.OrbitalData = Asteroid.OrbitalData
				asteroidStruct.Name = Asteroid.Name

				//A single asteroid has many dates it closes in on the earth, for each entry we fetch data and put in
				//to its respective box in the slice

				for i := range Asteroid.CloseApproachData {
					approachData.Date = Asteroid.CloseApproachData[i].CloseApproachDate
					approachData.Lunar = Asteroid.CloseApproachData[i].MissDistance.Lunar
					approachData.Kilometers = Asteroid.CloseApproachData[i].MissDistance.Kilometers
					approachData.Danger = functionality.DangerZone(
						Asteroid.EstimatedDiameter.Meters.EstimatedDiameterMax,
						Asteroid.EstimatedDiameter.Meters.EstimatedDiameterMin,
						Asteroid.CloseApproachData[i].RelativeVelocity.KilometersPerSecond,
						Asteroid.CloseApproachData[i].MissDistance.Kilometers,
					)
					asteroidStruct.CloseApproachData = append(asteroidStruct.CloseApproachData, approachData)
				}

				//Sorts the slice on which entry has the biggest danger value
				sort.Slice(asteroidStruct.CloseApproachData, func(i, j int) bool {
					return asteroidStruct.CloseApproachData[i].Danger > asteroidStruct.CloseApproachData[j].Danger
				})

				//prints to site
				w.Header().Add("content-type", "application/json")
				err = json.NewEncoder(w).Encode(asteroidStruct)
				if err != nil {
					http.Error(w, "Decoding failed, try again", http.StatusBadRequest)
				}

			} else {
				http.Error(w, "Malformed request to NASA api, please refer to the user manual on how to format requests.", http.StatusBadRequest)
			}
		} else {
			http.Error(w, "Http Method not allowed, Get only", http.StatusBadRequest)

		}
	}
	return http.HandlerFunc(f)
}
