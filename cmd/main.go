package main

import (
	"NasaProject/handlers"
	"fmt"
	"log"
	"net/http"
)

// 64NQkSQzqktiHMecqzfS0cFTGA89WyTKsFDb6gx3

func defaultHandler(w http.ResponseWriter, r *http.Request) {
	_, _ = fmt.Fprintf(w, "Hello, this is the default page for our Nasa prosject\n"+
		"Please use one of the following:\n"+
		"/nasaSquad/v1/Chuck\n"+
		"/nasaSquad/v1/Neo\n"+
		"/nasaSquad/v1/Id")
}

func main() {

	key := "64NQkSQzqktiHMecqzfS0cFTGA89WyTKsFDb6gx3"

	http.HandleFunc("/", defaultHandler)
	http.Handle("/nasaSquad/v1/Chuck/", handlers.ChuckHandler())
	http.Handle("/nasaSquad/v1/Neo/", handlers.NeoHandler(key))
	http.Handle("/nasaSquad/v1/Id/", handlers.IDHandler(key))
	println("listening on ", 8080)

	log.Fatal(http.ListenAndServe(":8080", nil))
}
