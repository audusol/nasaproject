# Start from the latest golang base image
FROM golang:latest

# Set the Current Working Directory inside the container
WORKDIR /app

ADD . . 

# Build the Go app
RUN go build -o main cmd/main.go

# Command to run the executable
ENTRYPOINT ["/app/main"]
