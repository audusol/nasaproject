__Group 9: Scuffed__

Group members: 

* Audun Landøy Solli 

* Stian André Høydal 

* Stig Thomas Gulbrandsen 

* Tea Høvik Knudsen 

Project name: The Nasa Squad

__Description__     
Project in Cloud Technologies IMT2681 regarding the Nasa API about Near Earth Object (neo).     
Using the NASA API we have aggregated and parsed data and delivered a threat matrix of 
nearby death-rocks to the user.     
You can also look up a specific asteroid by id and see when this asteroid is going to be 
at it's most dangerous.     
In addition we have also used the Chuck Norris API to aggregate a random Chuck Norris joke 
within a random category. 

We managed to stay at our original project plan, but there were some hard aspects.  
Some of the hard aspects of the project was understanding the API that was given from Nasa. 
We got in contact with someone from support as Nasa, who answered some of the questions that 
we had. It turned out that the IDs were dynamic, which made things some what more complicated. 
After some time, we finally managed to figure out a solution that worked. 

__Time used at project__        
We started with our project on the 6th of November. After this we met up as a group to work
with the project. Some of the work is done individually, but mostly we have worked together.    
We have kept an time log that shows approximately how much work that has been contributed from 
each group member, and in total we have used 75 working hours at this project.


__Endpoints used in this project:__     
/Neo/ will by default give you a list of asteroids for the following week, sorted by danger.    
/Neo/?start_date=&end_date= will give you a list of asteroids within time frame set by the user.        
NB: it is only possible to have a time frame that covers a week or less.        
Example: /nasaSquad/v1/Neo/?start_date=2020-01-01&end_date=2020-01-08

/Id/< Chosen id > will return the id, name, absolute magnitude and a list of that specific 
asteroid close approach data. The list of close approach data will be sorted on danger, and will get data from both
the past and in the future.     
Absolute magnitude will be based on how bright the asteroids is/will be from the Earth.     
Example: /nasaSquad/v1/Id/2004953       
This example will return absolute magnitude of 14.1 when it is at its highest.      
To compare, the sun has a absolute magnitude of 4.9.

/Chuck/ will generate a random Chuck Norris joke within a random category.        
It is also possible to get a random Chuck Norris joke within a specific category.       
/Chuck/< Chosen category > will provide this. The different categories to choose from is:
* animal 

* career 

* celebrity 

* dev 

* explicit 

* fashion 

* food 

* history 

* money 

* movie 

* music 

* political 

* religion 

* science 

* sport 

* travel 


From the assignment criteria, we have chosen to satisfy the criteria of using OpenStack, Docker
and two different APIs to aggregate and parse data.     
In addition, the program is written in Go.          
The service is deployed as ia Docker image, please use the following for testing: 
http://10.212.139.158:8080/nasaSquad/v1/ 