package functionality

//Total list of asteroids
type AsteroidList struct {
	ElementCount    int                   `json:"element_count"`
	NearEarthObject map[string][]Asteroid `json:"near_earth_objects"`
}

type Meters struct {
	EstimatedDiameterMin float64 `json:"estimated_diameter_min"`
	EstimatedDiameterMax float64 `json:"estimated_diameter_max"`
}

type EstimatedDiameter struct {
	Meters Meters `json:"meters"`
}

type RelativeVelocity struct {
	KilometersPerSecond string `json:"kilometers_per_second"`
	KilometersPerHour   string `json:"kilometers_per_hour"`
}

type MissDistance struct {
	Lunar      string `json:"lunar"`
	Kilometers string `json:"kilometers"`
}

//Close Apporach Data of the asteroid
type CloseApproachData struct {
	CloseApproachDate string           `json:"close_approach_date"`
	RelativeVelocity  RelativeVelocity `json:"relative_velocity"`
	MissDistance      MissDistance     `json:"miss_distance"`
	OrbitingBody      string           `json:"orbiting_body"`
}

//The asteroid itself
type Asteroid struct {
	ID                             string              `json:"id"`
	EstimatedDiameter              EstimatedDiameter   `json:"estimated_diameter"`
	IsPotentiallyHazardousAsteroid bool                `json:"is_potentially_hazardous_asteroid"`
	CloseApproachData              []CloseApproachData `json:"close_approach_data"`
}

//IDAsteroid has data for asteroids taken from the ID api
type IDAsteroid struct {
	ID                             string              `json:"id"`
	Name                           string              `json:"name"`
	AbsoluteMagnitude              float64             `json:"absolute_magnitude_h"`
	EstimatedDiameter              EstimatedDiameter   `json:"estimated_diameter"`
	IsPotentiallyHazardousAsteroid bool                `json:"is_potentially_hazardous_asteroid"`
	CloseApproachData              []CloseApproachData `json:"close_approach_data"`
	OrbitalData                    OrbitalData         `json:"orbital_data"`
}

//OrbitClass is an orbital class
type OrbitClass struct {
	OrbitClassType        string `json:"orbit_class_type"`
	OrbitClassDescription string `json:"orbit_class_description"`
	OrbitClassRange       string `json:"orbit_class_range"`
}

//OrbitalData has orbital data
type OrbitalData struct {
	OrbitID                   string     `json:"orbit_data"`
	OrbitDeterminationDate    string     `json:"orbit_determination_date"`
	LastObservationDate       string     `json:"last_observation_date"`
	DataArcInDays             int64      `json:"data_arc_in_days"`
	ObservationsUsed          int64      `json:"observations_used"`
	OrbinUncertainty          string     `json:"orbit_uncertainty"`
	MinimumOrbitIntersection  string     `json:"minimum_orbit_intersection"`
	JupiterTisserandInvariant string     `json:"jupiter_tisserand_invariant"`
	EpocOsculation            string     `json:"epoch_osculation"`
	Eccentricity              string     `json:"eccentricity"`
	SemiMajorAxis             string     `json:"semi_major_axis"`
	Inclination               string     `json:"inclination"`
	AscendingNodeLongitude    string     `json:"ascending_node_longitude"`
	OrbitalPeriod             string     `json:"orbital_period"`
	PerihelionDistance        string     `json:"perihelion_distance"`
	PerihelionArgument        string     `json:"perihelion_argument"`
	AphelionDistance          string     `json:"aphelion_distance"`
	PerihelionTime            string     `json:"perihelion_time"`
	MeanAnomaly               string     `json:"mean_anomaly"`
	MeanMotion                string     `json:"mean_motion"`
	Equinox                   string     `json:"equinox"`
	OrbitClass                OrbitClass `json:"orbit_class"`
}

//Asteroid info for the normal Neo handler
type InfoAsteroids struct {
	ID         string  `json:"id"`
	Date       string  `json:"date"`
	Danger     float64 `json:"danger"`
	Lunar      string  `json:"lunar"`
	Kilometers string  `json:"kilometers"`
}

//Asteroid info for the ID handler
type InfoIDAsteroids struct {
	ID                string           `json:"id"`
	Name              string           `json:"name"`
	AbsoluteMagnitude float64          `json:"absolute_magnitude"`
	CloseApproachData []IDApproachData `json:""`
	OrbitalData       OrbitalData      `json:"orbital_data"`
}

//Approach data for the ID handler
type IDApproachData struct {
	Date       string  `json:"date"`
	Lunar      string  `json:"lunar"`
	Kilometers string  `json:"kilometers"`
	Danger     float64 `json:"danger"`
}

//Encoding struct
type Encoding struct {
	ListAsteroids []InfoAsteroids `json:"list_asteroids"`
}

//ChuckData json data
type ChuckData struct {
	Categories []string `json:"categories"`
	CreatedAt  string   `json:"created_at"`
	UpdatedAt  string   `json:"updated_at"`
	IconURL    string   `json:"icon_url"`
	ID         string   `json:"id"`
	URL        string   `json:"url"`
	Value      string   `json:"value"`
}

//ChuckJoke ...
type ChuckJoke struct {
	Categories []string `json:"categories"`
	IconURL    string   `json:"icon_url"`
	ID         string   `json:"id"`
	URL        string   `json:"url"`
	Value      string   `json:"value"`
}
