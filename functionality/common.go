package functionality

import (
	"net/http"
	"strconv"
)


func GetDates(r *http.Request) (string, string) {
	// Gets start date from user
	startDate := ""
	if r.URL.Query()["start_date"] != nil {
		startLimit := r.URL.Query()["start_date"][0]
		startDate = startLimit
	}

	// Gets end date from user
	endDate := ""
	if r.URL.Query()["end_date"] != nil {
		endLimit := r.URL.Query()["end_date"][0]
		endDate = endLimit
	}
	return startDate, endDate
}

//dangerZone calculates a danger for a given asteroid with it's information sent to the function.
func DangerZone(max, min float64, s, d string) float64 {
	size := (max + min) / 2                                 // takes the median value of speed
	speed, _ := strconv.ParseFloat(s, 64)                   // finds speed
	distance, _ := strconv.ParseFloat(d, 64)                // finds distance
	danger := (size * size * speed) / (distance * distance) // calculates danger
	return danger * 1000000                                 // since danger is so small, multiply with 1000000 to make the number more relatable
}

//GetID gets the id ¯\_(ツ)_/¯
func GetID(r *http.Request) string {
	if len(r.URL.Path) >= 17 {
		ID := r.URL.Path[17:]
		return ID
	}
	//Else return blank
	return ""
}
